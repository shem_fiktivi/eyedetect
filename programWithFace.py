import cv2
import pickle


def main():
    with open('./PickleRick.p', mode='rb') as p:
        known_people = pickle.load(p)

    recognizer = FaceRecognation(knowm_people)

    process_this_frame = True
    cap = cv2.VideoCapture("./drive/My Drive/unknown.mp4")

    videodims = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
                 int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video = cv2.VideoWriter("./drive/My Drive/known6.mp4", fourcc, int(cap.get(cv2.CAP_PROP_FPS)), videodims)

    success, frame = cap.read()
    while success:
        faces_and_names = recognizer.recognize(frame)
        frame_with_faces = recognizer.draw(frame, faces_and_names)

        video.write(frame_with_faces)
        success, frame = cap.read()
    video.release()


if __name__ == "__main__":
    main()