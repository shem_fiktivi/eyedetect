import cv2

class FaceRecognation:
    def __init__(self, known_people):
        self.people = known_people


    def recognize(self, frame):
        face_locations = face_recognition.face_locations(frame, model='cnn')
        face_encodings = face_recognition.face_encodings(frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(known_people.keys(), face_encoding)

            name = "unknown"

            # Or instead, use the known face with the smallest distance to the new face
            face_distances = face_recognition.face_distance(known_people.keys(), face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_people[best_match_index]

            face_names.append(name)
        return zip(face_locations, face_names)


    def draw(self, frame, to_recognize):
        for (top, right, bottom, left), name in to_recognize:
            cv2.rectangle(frame, (left, bottom - 140), (right, bottom), (0, 0, 255))
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
        return frame