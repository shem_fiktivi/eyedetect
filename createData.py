import face_recognition
import numpy as np
from PIL import Image
import pickle

# Load a sample picture and learn how to recognize it.
dorin_image = np.rot90(face_recognition.load_image_file("./Dorin.jpg"), 3)
dorin_face_encoding = face_recognition.face_encodings(dorin_image, model='large')[0]

# Load a second sample picture and learn how to recognize it.
ido_image = np.rot90(face_recognition.load_image_file("./Ido Moskovitz.jpg"), 3)
ido_face_encoding = face_recognition.face_encodings(ido_image, model='large')[0]

# Create dict of known face encodings and their names
faces = {
    ido_face_encoding: "Ido Moskovitz",
    dorin_face_encoding: "Doreen Nicole"
}

with open('./drive/My Drive/PickleRick.p', mode='wb+') as p:
  pickle.dump(faces, p)